let isSubscribed = false;
let swRegistration = null;
let applicationKey = "BK3Q7j8fcGFws03RiU5XakzDJ7KGEiRhdIX2H5U8eNmhhkdHT_j0_SD09KL96aFZOH_bsjr3uRuQPTd77SRP3DI";


// Url Encription
function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

// Uninstall service worker
// navigator.serviceWorker.getRegistrations().then(function(registrations) {
//  for(let registration of registrations) {
//   registration.unregister()
// } })

//Installing service worker
if ('serviceWorker' in navigator && 'PushManager' in window) {
    console.log('Service Worker and Push is supported');

    navigator.serviceWorker.register('/js/sw.js')
        .then(function (swReg) {
            console.log('service worker registered');

            swRegistration = swReg;

            swRegistration.pushManager.getSubscription()
                .then(function (subscription) {

                	console.log("in this subscription");
                	console.log(subscription);
                    isSubscribed = !(subscription === null);

                    if (isSubscribed) {
                        console.log('isSubscribed');
                    } else {
                    	console.log("subscription value null");

                        swRegistration.pushManager.subscribe({
                                userVisibleOnly: true,
                                applicationServerKey: urlB64ToUint8Array(applicationKey)
                            })
                            .then(function (subscription) {
                                
                                console.log('User is subscribed');
                                console.log(subscription);

                                saveSubscription(subscription);

                                isSubscribed = true;
                            })
                            .catch(function (err) {
                                console.log('Failed to subscribe user: ', err);
                            })
                    }
                })
        })
        .catch(function (error) {
            console.error('Service Worker Error', error);
        });
} else {
    console.warn('Push messaging is not supported');
}

// Send request to database for add new subscriber
function saveSubscription(subscription) {
    let xmlHttp = new XMLHttpRequest();
    //put here API address
    xmlHttp.open("POST", "/subscribe");
    xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState != 4) return;
        if (xmlHttp.status != 200 && xmlHttp.status != 304) {
            console.log('HTTP error ' + xmlHttp.status, null);
        } else {
            console.log("User subscribed to server");
        }
    };

    xmlHttp.send(JSON.stringify(subscription));
}


// function saveSubscription(subscription){
// 	console.log("======== in ========");
// 	console.log(JSON.stringify(subscription));
// 	$.ajax({
// 		type: "POST",
// 		url: "/subscribe",
// 		data: {data: '2222222'},
// 		timeout: 3000,
// 		success: function(data){
// 			console.log(data);
// 		}
// 	});
// }


	// $.ajax({
	// 	type: "POST",
	// 	url: "/subscribe",
	// 	data: {data: $('input[name=addressSelect]:checked').val(), _csrf: '#{csrfToken}'},
	// 	timeout: 3000,
	// 	success: function(data){
	// 		if(data.success==1){
	// 			window.location.href = "/checkout";
	// 		}
	// 	}
	// });


