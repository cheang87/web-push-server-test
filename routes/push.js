const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
var Subscription = require('../models/subscribers/subscribers');
const webPush = require('web-push');
const keys = require('../config/keys');

//Post route of push url is as http://host:3000/push
router.get('/', (req, res) => {
    // const payload = {
    //   "title": "aaa aa 1",
    //   "message": "aa a aa2",
    //   "url": "aaaaa3",
    //   "ttl": "aaaaa4",
    //   "icon": "aaaaa5",
    //   "image": "aaaaa6",
    //   "badge": "aaaaa7",
    //   "tag": "aaaaa8"
    // };

    const payload = "testing";

    Subscription.findOne({},function(err,subscription){
        console.log('============== subscriptions ========================');
        if (err) {
            console.error(`Error occurred while getting subscriptions`);
            res.status(500).json({
                error: 'Technical error occurred'
            });
        } else {
            console.log('============== subscriptions else ========================');
            const pushSubscription = {
                endpoint: subscription.endpoint,
                keys: {
                    p256dh: subscription.keys.p256dh,
                    auth: subscription.keys.auth
                }
            };

            const pushPayload = JSON.stringify(payload);
            const pushOptions = {
                vapidDetails: {
                    subject: "http://example.com",
                    privateKey: keys.privateKey,
                    publicKey: keys.publicKey
                },
                TTL: payload.ttl,
                headers: {}
            };

            webPush.sendNotification(
                pushSubscription,
                pushPayload,
                pushOptions
            ).then((value) => {
                console.log("============== in  value ============");
                console.log(value);
            }).catch((err) => {
                console.log("============== in  err ============");
                console.log(err);
            });
            res.json({
                data: 'Push triggered'
            });
        }
    })
});

// fixed the error get request for this route with a meaningful callback
// router.get('/', (req, res) => {
//     res.json({
//         data: 'Invalid Request Bad'
//     });
// });
module.exports = router;