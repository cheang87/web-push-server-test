let notificationUrl = '';

//notification registered feature for getting update automatically from server api
self.addEventListener('push', function (event) {
    console.log('Push received:11111');
    console.log('Push received: ', event);
    let _data = event.data ? JSON.parse(event.data.text()) : {};
    console.log("====== _data =======");
    console.log(_data);
    notificationUrl = _data.url;
    // var options = {
    //     body: 'Here is a notification body!',
    //     icon: 'images/example.png',
    //     vibrate: [100, 50, 100],
    //     data: {
    //       dateOfArrival: Date.now(),
    //       primaryKey: 1
    //     },
    //     actions: [
    //       {action: 'explore', title: 'Explore this new world',
    //         icon: 'images/checkmark.png'},
    //       {action: 'close', title: 'Close notification',
    //         icon: 'images/xmark.png'},
    //     ]
    //   };
      
    event.waitUntil(
        // self.showNotification('Hello world!', options);
        self.registration.showNotification(_data.title, {
            body: _data.message,
            icon: _data.icon,
            tag: _data.tag
        })
    );
});




//notification url redirect event click
self.addEventListener('notificationclick', function (event) {
    event.notification.close();

    event.waitUntil(
        clients.matchAll({
            type: "window"
        })
        .then(function (clientList) {
            if (clients.openWindow) {
                return clients.openWindow(notificationUrl);
            }
        })
    );
});
