var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var webpush = require('web-push');

// var csrfProtection = csrf({cookie: true});
// router.use(csrfProtection);

//Authorized START//

router.get('/logged', isLoggedIn, function(req, res, next) {
    res.render('modules/signin/index', {title: 'Home'});
});

//Authorized END//

//Not Authorized START//

router.use('/', notLoggedIn, function(res, req, next) {
    next();
});

router.get('/', function(req, res, next) {
    res.render('index', {title: 'DigiCore JS'});
});

router.get('/webpush', function(req, res, next) {
    var vapidKeys = webpush.generateVAPIDKeys();
    console.log("publicKey :"+vapidKeys.publicKey);
    console.log("privateKey :"+vapidKeys.privateKey);
    res.render('index', {title: 'DigiCore JS'});
});

router.get('/404', function(req, res, next) {
    res.render('404', {title: 'DigiCore JS'});
});

//Not Authorized END//

module.exports = router;

function isLoggedIn (req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function notLoggedIn (req, res, next) {
    if (!req.isAuthenticated()) {
        return next();
    }
    res.redirect('/logged');
}